package com.template.Repository.Basic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.template.Model.Basic.Role;


@Repository	 
public interface RoleRep extends JpaRepository<Role,Integer>{

	@Query("Select role from Role role where role.status !=1 and role.ID=:id")
	public Role findRoleById(@Param("id") int id);

}
