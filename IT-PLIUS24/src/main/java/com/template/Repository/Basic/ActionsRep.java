package com.template.Repository.Basic;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.template.Model.Basic.Action;

@Repository
public interface ActionsRep  extends JpaRepository<Action,Integer>{

	@Query("Select a from Action a where a.action_name_en=:name ")
	public Action getActionByname(@Param("name") String name);
	@Query("Select a from Action a where a.ID=:id ")
	public Action findActionById(@Param("id") int id);

}

