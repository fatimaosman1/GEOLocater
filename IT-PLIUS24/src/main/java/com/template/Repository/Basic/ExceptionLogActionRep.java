package com.template.Repository.Basic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.template.Model.Basic.ExceptionLogAction;

@Repository
public interface ExceptionLogActionRep extends JpaRepository<ExceptionLogAction,Integer>{
	

	
	@Query("Select l from ExceptionLogAction l ORDER BY l.action_date DESC")
	public List<ExceptionLogAction> getAllExLogActions();

}
