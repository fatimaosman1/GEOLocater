package com.template.Service.Basic;


import com.template.Repository.Basic.UsersRep;
import com.template.domain.Exception.NoDataFoundException;
import com.template.Model.Basic.User;
import java.util.Arrays;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

	private final UsersRep userRep;

	public User findById(int id) {
		return userRep.findById(id).get();
	}
	public User findByUserName(String username) {
		return userRep.findByusername(username);
	}
	public User save(User user) {
		return userRep.save(user);
	}


	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRep.findByusername(username);
		GrantedAuthority authority = new SimpleGrantedAuthority(user.getUser_role().getRole_name());
		if (user == null)
			throw new NoDataFoundException("No User Found");
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), Arrays.asList(authority));

	}


}
