package com.template.Service.Basic;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.template.domain.Exception.NoDataFoundException;
import com.template.Model.Basic.Action;
import com.template.Repository.Basic.ActionsRep;

@Service
@RequiredArgsConstructor
public class ActionService  {

	private final ActionsRep actionsRep;
	
	public List<Action> findAll() {
		List<Action> actionList = actionsRep.findAll();
		

		return actionList;
	}
	
	public Action findActionById(int id) {
		Action action = actionsRep.findActionById(id);
		if(action == null)
			throw new NoDataFoundException("No action with ID : "+id);
		return action;
	}
	public Action save(Action action) {

		return actionsRep.save(action);
	}
	
	
	}
	
	

