package com.template.Service.Basic;


import java.time.ZoneId;
import java.sql.Date;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.template.Model.Basic.ExceptionLogAction;
import com.template.Repository.Basic.ExceptionLogActionRep;

@Service
@RequiredArgsConstructor
public class LogService {

	private final ExceptionLogActionRep exceptionLogActionRep;

	public void addExLogAction(int action_index, String param) {
		ExceptionLogAction a = null;
		a = new ExceptionLogAction(
				Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()), action_index, param);

		exceptionLogActionRep.save(a);

	}
}
