package com.template.Service.Main;

import com.template.domain.DTO.request.GeoLocatorRequest;
import com.template.domain.DTO.response.GeoLocatorResponse;
import com.template.domain.DTO.response.MailResponse;
import com.template.domain.Exception.NoDataFoundException;
import com.template.Model.Main.GeoLocator;
import com.template.Repository.Main.GeoLocatorRep;
import lombok.RequiredArgsConstructor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.*;

@Service
@RequiredArgsConstructor
public class GeoLocatorService {

	@Autowired
	private  GeoLocatorRep geoRep;
	@PersistenceContext
	private EntityManager entityManager;
	private  final String GEOCODING_RESOURCE = "https://geocode.maps.co/search";
	private GeoLocatorRequest replaceWiteSpace(GeoLocatorRequest request){
		if(request.getCity()!=null)	request.setCity(request.getCity().replace(' ','+'));
		if(request.getCountry()!=null)request.setCountry(request.getCountry().replace(' ','+'));
		if(request.getPostalcode()!=null)request.setPostalcode(request.getPostalcode().replace(' ','+'));
		if(request.getState()!=null)request.setState(request.getState().replace(' ','+'));
		if(request.getStreet()!=null)request.setStreet(request.getStreet().replace(' ','+'));
		return request;
	}
	private List<GeoLocator> findGeoLocatorFromDB(GeoLocatorRequest request) {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM geo_locator e WHERE 1=1 ");
		if (request.getCity() != null) {
			sql.append("AND e.city LIKE :city ");
			params.put("city", "%" + request.getCity() + "%");
		}
		if (request.getCountry() != null) {
			sql.append("AND e.country LIKE :country ");
			params.put("country", "%" + request.getCountry() + "%");
		}
		if (request.getPostalcode() != null) {
			sql.append("AND e.postalcode LIKE :postalcode ");
			params.put("postalcode", "%" + request.getPostalcode() + "%");
		}
		if (request.getState() != null) {
			sql.append("AND e.state LIKE :state ");
			params.put("state", "%" + request.getState() + "%");
		}
		if (request.getStreet() != null) {
			sql.append("AND e.street LIKE :street ");
			params.put("street", "%" + request.getStreet() + "%");
		}



		Query query = entityManager.createNativeQuery(sql.toString(), GeoLocator.class);
		for (Map.Entry<String, Object> param : params.entrySet()) {
			query.setParameter(param.getKey(), param.getValue());
		}
		return query.getResultList();
	}

	private GeoLocator save(GeoLocator g) {return geoRep.save(g);}

	private  String findGeoLocatorFromThirdParty(GeoLocatorRequest request) throws IOException, InterruptedException {

		HttpClient httpClient = HttpClient.newHttpClient();
		String requestUri = GEOCODING_RESOURCE;
		request=this.replaceWiteSpace(request);
		boolean flag=false;
		if (request.getCity() != null) {
			requestUri+="?city="+request.getCity();
			flag=true;
		}
		if (request.getCountry() != null) {
			if (flag)requestUri+="&country="+request.getCountry();else requestUri+="?country="+request.getCountry();
			flag=true;
		}
		if (request.getPostalcode() != null) {
			if (flag)requestUri+="&postalcode="+request.getPostalcode();else requestUri+="?postalcode="+request.getPostalcode() ;
			flag=true;
		}
		if (request.getState() != null) {
			if (flag)requestUri+="&state="+request.getState();else requestUri+="?state="+request.getState();
			flag=true;
		}
		if (request.getStreet() != null) {
			if (flag) requestUri+="&street="+request.getStreet();else  requestUri+="?street="+request.getStreet();
			flag=true;
		}
		HttpRequest geocodingRequest = HttpRequest.newBuilder()
				.GET()
				.uri(URI.create(requestUri))
				.timeout(Duration.ofMillis(2000)).build();

		HttpResponse geocodingResponse = httpClient.send(geocodingRequest, HttpResponse.BodyHandlers.ofString());

		return geocodingResponse.body().toString();
	}
	private  GeoLocatorResponse convertToDto(GeoLocator g){
		return  new GeoLocatorResponse(g.getID(),g.getClazz(),g.getType(),g.getAddress());
	}
	private MailResponse convertToMailDto(GeoLocator g){
		return  new MailResponse(g.getClazz(),g.getType(),g.getAddress());
	}
	public List<GeoLocatorResponse> findGeoLocator(GeoLocatorRequest request) throws IOException, InterruptedException {
		List<GeoLocatorResponse> result=new ArrayList<>();
		List<GeoLocator> list=this.findGeoLocatorFromDB(request);
		if (list.isEmpty()){
			String response=this.findGeoLocatorFromThirdParty(request);

			ObjectMapper mapper = new ObjectMapper();
			JsonNode responseJsonNode = mapper.readTree(response);
			for (int i=0;i<responseJsonNode.size();i++){
				GeoLocator g=new GeoLocator();
				g.setAddress(responseJsonNode.get(i).get("display_name").asText());
				g.setClazz(responseJsonNode.get(i).get("class").asText());
				g.setType(responseJsonNode.get(i).get("type").asText());
				g.setCounty(request.getCountry());
				g.setCity(request.getCity());
				g.setState(request.getState());
				g.setPostalcode(request.getPostalcode());
				g.setStreet(request.getStreet());
				this.save(g);
				result.add(this.convertToDto(g));
			}
			if(responseJsonNode.isEmpty()){
				throw new NotFoundException("the Geo Locator not found");
			}


		}else {
            for (GeoLocator geoLocator : list) {
                result.add(this.convertToDto(geoLocator));
            }
		}
		return result;
	}

	public String findbyids(List<Integer>ids){
		List<GeoLocator> list=geoRep.findAllById(ids);
		String result="";
		for (GeoLocator geoLocator : list) {
			result+="\n Address: "+geoLocator.getAddress()+"\n class: "+geoLocator.getClazz()+"\n Type: "+geoLocator.getType()+" \n\n";
		}
		return result;
	}
	}
	
	

