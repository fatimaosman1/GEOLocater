package com.template.Advisor;

//import com.template.Service.Basic.LogService;

import com.template.Service.Basic.LogService;
import com.template.domain.Enum.ExceptionLogName;
import com.template.domain.Exception.NoDataFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.MethodNotAllowedException;


@ControllerAdvice(basePackages ="com.template.RestController")
public class ControllerAdvisor {
	@Autowired
	public LogService logActionService;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public  ResponseEntity<ErrorResponse>  handleNodataFoundException(NoDataFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
		logActionService.addExLogAction(
				ExceptionLogName.NoDataFoundException.ordinal(), ex.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}


	@ExceptionHandler(MethodNotAllowedException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ResponseEntity<ErrorResponse> MethodNotAllowedException(NotFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.METHOD_NOT_ALLOWED, ex.getMessage());
		logActionService.addExLogAction(
				ExceptionLogName.MethodNotAllowedException.ordinal(), ex.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.METHOD_NOT_ALLOWED);
	}
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public  ResponseEntity<ErrorResponse>  InternalServerErrorException(Exception ex) {
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		logActionService.addExLogAction(
				ExceptionLogName.InternalServerErrorException.ordinal(), ex.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}


}
