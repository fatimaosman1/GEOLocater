package com.template.domain.Exception;

public class InternalServerErrorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InternalServerErrorException(String param) {

        super(param);
    }
}