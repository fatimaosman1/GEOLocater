package com.template.domain.DTO.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GeoLocatorResponse {

    private int id;
    private String Clazz;
    private String type;
    private String address;


}
