package com.template.domain.DTO.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class RegisterResponseDTO {

    private String username;
    private String email;
    private int id ;
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    private Date created;

    public RegisterResponseDTO(String username) {
        this.username = username;
        this.email = email;
        this.created = new Date();
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
