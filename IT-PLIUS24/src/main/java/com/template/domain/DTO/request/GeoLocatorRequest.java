package com.template.domain.DTO.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GeoLocatorRequest {

    private String street;
    private String country;
    private String city;
    private String postalcode;
    private String state;


}
