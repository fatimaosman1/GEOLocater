package com.template.domain.DTO.request;




public class SignRequestDTO {

    private String username;
    private String password;

    public SignRequestDTO() {
        super();
    }
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }

}
