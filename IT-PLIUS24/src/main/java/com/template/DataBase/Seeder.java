package com.template.DataBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;


import com.template.DataBase.Seeders.ActionSeeder;
import com.template.DataBase.Seeders.RoleActionSeeder;
import com.template.DataBase.Seeders.RoleSeeder;
import com.template.DataBase.Seeders.UserSeeder;
import com.template.Repository.Basic.ActionsRep;
import com.template.Repository.Basic.RoleActionRep;
import com.template.Repository.Basic.RoleRep;
import com.template.Repository.Basic.UsersRep;



@Component
public class Seeder implements CommandLineRunner {
	
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String schema;

	@Value("${sedeer.status}")
	private String status;

	private final Log logger = LogFactory.getLog(Seeder.class);
	
	@Autowired
	RoleRep roleRep ;
	
	@Autowired
	UsersRep userRep;

	@Autowired
	ActionsRep actionsRep;
	
	@Autowired
	RoleActionRep roleActionRep;
	
	@Autowired
	ActionsRep actionRep;
	


	public void run(String... strings) {
		logger.info("Seeder ==> Status DB:"+schema);
		if(status.equalsIgnoreCase("false")){
			logger.info("Seeder: Stoped.");
			return;
		}

		logger.info("Seeder: Running.");
		if(schema.equalsIgnoreCase("create")) {
			//when Status Data Base:create in application.properties
			create();
		}else if(schema.equalsIgnoreCase("update")) {
			//when Status Data Base:update in application.properties
			update();
		}else if(schema.equalsIgnoreCase("create-drop")) {
			create_drop();
		}else {
			all();
		}

		logger.info("Seeder:Successfully Seeder.");
	}
	
	public void create() {
		
		new ActionSeeder().run(actionsRep);
		new RoleSeeder().run(roleRep);
		new RoleActionSeeder().run(roleRep,roleActionRep,actionRep);
		new UserSeeder().run(roleRep,userRep,roleActionRep);

		
	}
	
	public void update() {
		
	}
	
	public void create_drop() {
		
	}
	
	public void all() {
		
	}

	
}


