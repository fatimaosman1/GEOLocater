package com.template.DataBase.Seeders;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.template.DataBase.Seeder;
import com.template.Model.Basic.Action;
import com.template.Repository.Basic.ActionsRep;

public class ActionSeeder {

	@Autowired
	ActionsRep actionsRep;

	private Log logger = LogFactory.getLog(Seeder.class);

	public ActionSeeder() {
		logger.info("	==>Seeder ==> " + getClass().getSimpleName());
	}

	public void run(ActionsRep actionsRep) {

		Action action = new Action();


		action.setAction_name_en("login");
		action.setDescription("description");

		actionsRep.save(action);

		// ======
		action = new Action();
		action.setAction_name_en("logout");
		action.setDescription("description");

		actionsRep.save(action);



		
	}

}
