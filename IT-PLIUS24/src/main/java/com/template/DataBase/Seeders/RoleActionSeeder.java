package com.template.DataBase.Seeders;

import java.util.ArrayList;
import java.util.List;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.template.DataBase.Seeder;
import com.template.Model.Basic.Action;
import com.template.Model.Basic.Role;
import com.template.Model.Basic.RoleAction;
import com.template.Repository.Basic.ActionsRep;
//import com.template.Repository.Basic.RoleActionOppositeRep;
import com.template.Repository.Basic.RoleActionRep;
import com.template.Repository.Basic.RoleRep;

public class RoleActionSeeder {

	private final Log logger = LogFactory.getLog(Seeder.class);
	
	public RoleActionSeeder() {
		logger.info("	==>Seeder ==> "+ getClass().getSimpleName());
	}
	
	public void run(RoleRep roleRep,RoleActionRep roleActionRep,ActionsRep actionRep) {
		
		Role role;
		List<Action>actions;
		RoleAction roleAction;

		
		role=roleRep.findById(1).get();
		actions=new ArrayList<Action>();
		actions=actionRep.findAll();

		for(Action action : actions) {
			roleAction=new RoleAction();
			roleAction.setRole(role);
			roleAction.setAction(action);
			roleAction.setStatus(0);
			roleActionRep.save(roleAction);

		}
		
		


	}
	
}
