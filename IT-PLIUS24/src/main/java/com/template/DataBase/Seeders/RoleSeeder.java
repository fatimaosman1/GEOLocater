package com.template.DataBase.Seeders;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.template.DataBase.Seeder;
import com.template.Model.Basic.Role;
import com.template.Repository.Basic.RoleRep;



public class RoleSeeder {
	
	
	private final Log logger = LogFactory.getLog(Seeder.class);
	
	
	public RoleSeeder() {
		logger.info("	==>Seeder ==> "+ getClass().getSimpleName());
	}
	
	public void run(RoleRep roleRep) {
		
		Role role;
		
		role=new Role(1,"ADMIN", "description", 0, null, null);
		roleRep.save(role);
		
	}
}
