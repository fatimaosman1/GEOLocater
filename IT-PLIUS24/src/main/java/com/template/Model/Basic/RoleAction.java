package com.template.Model.Basic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Role_Action")
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@ToString
public class RoleAction {
	
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID ; 
	
	@ManyToOne( optional = true)
    @JoinColumn(name = "role_id",unique=false, referencedColumnName = "ID")
	private Role role ;
	
	@ManyToOne( optional = true)
    @JoinColumn(name = "action_id",unique=false, referencedColumnName = "ID")
	private Action action ;
	
	
	@Column(name="status")
	private int status;
	
	@Column(name = "CAN_DELETE")
	private int canDelete;


	
	
}
