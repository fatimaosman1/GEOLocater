package com.template.Model.Basic;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Role")
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor

public class Role {

	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID ; 
	
	@Column(name = "role_name")
	private String role_name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "status")
	private int status;
	
//	@JsonIgnore
	@OneToMany(targetEntity = User.class, mappedBy = "user_role", cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private List<User> users;

	@JsonIgnore
	@OneToMany(targetEntity = RoleAction.class, mappedBy = "role", cascade = CascadeType.ALL)
	private List<RoleAction> actions;
	@Override
	public String toString() {
		return "Role [ID=" + ID + ", role_name=" + role_name + ", description=" + description + ", status=" + status
				+ "]";
	}



	
	
}
