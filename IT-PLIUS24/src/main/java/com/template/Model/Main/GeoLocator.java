package com.template.Model.Main;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "GeoLocator")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GeoLocator {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	@Column(name = "street")
	private String street ;
	@Column(name = "country")
	private String county ;
	@Column(name = "city")
	private String city ;
	@Column(name = "postalcode")
	private String postalcode ;
	@Column(name = "state")
	private String state ;
	@Column(name = "class")
	private String Clazz ;
	@Column(name = "type")
	private String type ;
	@Column(name = "address")
	private String address ;



}
