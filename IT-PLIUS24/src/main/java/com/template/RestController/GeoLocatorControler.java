package com.template.RestController;

import com.template.Aspect.RestLog;
import com.template.Service.Main.MailSenderService;
import com.template.domain.DTO.request.GeoLocatorRequest;
import com.template.domain.DTO.request.MailRequestDTO;
import com.template.domain.DTO.request.SignRequestDTO;
import com.template.domain.DTO.response.GeoLocatorResponse;
import com.template.domain.DTO.response.MailResponse;
import com.template.domain.Exception.InternalServerErrorException;
import com.template.Model.Main.GeoLocator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.template.Service.Main.GeoLocatorService;
import java.util.List;
import java.util.Optional;
/**
 * this class is to define the Book Controller Requests.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class GeoLocatorControler {
	@Autowired
	public GeoLocatorService geoLocatorService;
	private final MailSenderService mailService;

	@RestLog(uri = "/api/GeoLocator")
	@PostMapping(value="/GeoLocator",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<List<GeoLocatorResponse>> getGeoLocator(@RequestBody GeoLocatorRequest request) {
		try {
			List<GeoLocatorResponse> list = geoLocatorService.findGeoLocator(request);;
			if (list.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.GATEWAY_TIMEOUT);
		}
	}

	@RestLog(uri = "/api/GeoLocator/SendMail")
	@PostMapping(value="/GeoLocator/SendMail",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<String> sendmail(@RequestBody MailRequestDTO request) {
		try {
		   mailService.sendNewMail(request.getMail(),"Geo Locator",geoLocatorService.findbyids(request.getIds()));
			return new ResponseEntity<>("Success", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Please Try Again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}




}


